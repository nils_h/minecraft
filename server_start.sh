TYPE=spigot # spigot OR paper
VERSION=1.14.4 #
DEBUG=false # setting this true will disable all updates and clears
WORLD_BACKUP=true # decides if current world is saved
PLUGINS_CLEAR=true # decides if the plugins are cleared
CONFIG_UPDATE=true # decides if config files and plugins are updated
JAR_UPDATE=true # decides if the binaries are updated

## V2 (AUTO-REBOOT AFTER STOP)
while true
do

find . -type f -iname "*.sh" -exec chmod +x {} \;

if [ $WORLD_BACKUP = true ]  && [ $DEBUG = false ]
then
./world_backup.sh
fi

if [ $PLUGINS_CLEAR = true ]  && [ $DEBUG = false ]
then
find plugins -maxdepth 1 -type f -iname "*.jar" -exec rm {} \;
fi

if [ $CONFIG_UPDATE = true ] && [ $DEBUG = false ]
then
./config_update.sh
fi

if [ $JAR_UPDATE = true ] && [ $DEBUG = false ]
then
./jar_update.sh
fi


## V2 (AUTO-REBOOT AFTER STOP)
./"$TYPE"-"$VERSION"_run.sh
echo "Server stopped. Rebooting in 10 seconds."
sleep 10
done


## V1 (DIRECT LAUNCH)
#./"$TYPE"-"$VERSION"_run.sh


## V3 (SCREEN)
# screen -dmS mc-server ./"$TYPE"-"$VERSION"_run.sh

## RE-OPEN SERVER INTERFACE
# screen -r mc-server

## TERMINATE SERVER INTERFACE
# screen -X -S mc-server quit && screen -wipe