BACKUP_UPLOAD=true # decides if backup is uploaded to OwnCloud
TIME_STAMP=$(date +"%Y_%m_%d_%I_%M_%p")

mkdir backups

zip -r backups/world_backup_$TIME_STAMP.zip world world_nether world_the_end

if [ $BACKUP_UPLOAD = true ]
then
./owncloud_upload.sh "Spiele/MCPC/worlds/server-2019" "backups/world_backup_$TIME_STAMP.zip" \
&& rm -f "backups/world_backup_$TIME_STAMP.zip"
fi